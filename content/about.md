---
title: "About"
date: 2021-01-21T16:57:46Z
draft: false
---

## The Goal

`[objc retain];` is here to show you that Objective-C still has a live and vibrant community, and that there's a place for your existing Objective-C code and your new projects. That place is…wherever you already are, thanks to free software!

[GNUstep](https://gnustep.org) has a long history as a free software (open source) implementation of Cocoa, the Objective-C APIs popularly used to create applications on the Mac. But with GNUstep, your ObjC can run on Linux, FreeBSD, Windows…wherever you and your users happen to be.

## The Stream

We [live-code Objective-C over on Twitch](https://www.twitch.tv/objcretain). You can [find previous episodes on the replay server](https://replay.objc-retain.com), mirrored [at our Peertube channel](https://peertube.co.uk/video-channels/objc_retain/videos) or [Youtube playlist](https://www.youtube.com/playlist?list=PLKMpKKmHd2SudZhgs0IQDMmTfbz7QFnYg). We stream tutorials, porting Cocoa apps, adding new features to GNUstep, and if you join the chat on Twitch you can have a say in what comes next!

See the [contact page](/contact) for details of how you can chat with us.

## Support

This stream is now entirely self-hosted and only mirrored to proprietary platforms to aid discovery; please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!

### Small Print

All content at `[objc retain];` is made available under the [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) license. The source code for the live stream player [is available here](https://github.com/iamleeg/rtmp.objc-retain.com), under the terms of the [GNU Affero General Public License, version 3](https://github.com/iamleeg/rtmp.objc-retain.com/blob/main/LICENSE).