+++
date = 2021-10-26T20:00:00Z
lastmod = 2021-10-26T20:00:00Z
author = "graham"
title = "Catching Up"
subtitle = "Recent episodes, and changes to future streams."
feature = "image/ep29.png"
+++

Wow, I'm really out of date here! I'm sorry about that. I won't bore you with most of the reasons, but one problem is that we've had a lot of trouble with Twitch not picking up the stream properly that's really disheartened me. And unfortunately we've found that [our own live player](https://rtmp.objc-retain.com) isn't reliable for everyone, so isn't a good alternative.

There are a couple of other options to evaluate. Fundamentally we want to make sure you're able to watch these episodes, and that other people can find them. That's what we care about most: sharing our enjoyment of GNUstep with the community. So over the next few weeks we're going to try different alternatives for sharing the stream, please check in here to find out what the plan is.

In the meantime, I haven't shared posts for the last few episodes. They're available over at [our replay server](https://replay.objc-retain.com). That's been working really well for us, so whatever we do for live streams you'll always be able to watch again over there.

Unfortunately episode 31 went so badly that we can't share it on replay. Very few people saw it, but those who did will know how frustrating it was. The week of objc shows are all up there now, and today's episode 32 is uploading as I write.
