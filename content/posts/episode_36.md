+++
date = 2022-02-11T17:00:00Z
lastmod = 2022-02-11T17:00:00Z
author = "graham"
title = "Episode 36"
subtitle = "Compiler chat; XUnit; Planning."
feature = "image/ep36.png"
+++

First off, an apology: I missed out on hitting record in OBS until we were about 25 minutes in so the first part of our chat is lost. If you're quick, you can catch it on the [Twitch replay](https://twitch.tv/objc-retain) for the first 14 days or so after recording. Sorry!

We talked quite a bit about what we want from GNUstep, what that means for what others want from GNUstep, and the struggles of working as part of a small community on a large project. Then we turned to our XCTest implementation. Or really, to somebody else's implementation; unfortunately it had unfavourable license terms so we were forced to avoid working on it.

This led us to think about why we were doing that work though, so we came up with the plan to make it easy to port Xcode-based ObjC projects to GNUstep-supporting platforms. Our first step is CocoaPods support, and we were pleasantly surprised to find that CocoaPods already works (or at least all of its tests pass) on Linux!

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-36).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
