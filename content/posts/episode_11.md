---
title: "Episode 11"
subtitle: "Apple and Freedom; even more SimpleAgenda"
author: "graham"
date: 2021-04-29T21:26:46+01:00
lastmod: 2021-07-14T12:10:00Z
draft: false
feature: image/ep11.png
---

We talk about how Apple do care about software freedom (of a sort), and we carry on working on SimpleAgenda. And I fix a configuration problem with [mattermost chat](https://chat.objc-retain.com) that unlocks the ability to use it for people who aren't me!

You can watch again on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-11/). The video is also mirrored on  [PeerTube](https://peertube.co.uk/videos/watch/9cfba06a-aff6-41e2-baf5-642f811b13d1) and [YouTube](https://youtu.be/B6HwzlvNaXg).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
