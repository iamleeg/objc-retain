+++
date = 2021-04-21T19:00:00Z
lastmod = 2021-07-14T19:00:00Z
author = "graham"
title = "Episode 10"
subtitle = "GNUstep, Apple, and Autonomy: SimpleAgenda"
feature = "image/ep10.png"
+++

We talk about how there are more dimensions to online autonomy than just free software licensing; some of the freedom-adjacent campaigns that have been (or should be) fought for citizens of a computer-heavy world; and GNUstep's unique place straddling both the free software and Apple proprietary communities.

Then we actually get down to brass tacks and start on the SimpleAgenda feature we were exploring last week: more flexible event durations.

You can watch again on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-10/). The video is also mirrored to [Peertube](https://peertube.co.uk/videos/watch/1039792b-58cf-4b13-84d9-5eec373fbe8e) and [Youtube](https://youtu.be/XwKSEPVCRq0).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
