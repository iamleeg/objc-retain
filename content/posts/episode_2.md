+++
date = 2021-02-17T21:00:00Z
lastmod = 2021-07-11T12:30:00Z
author = "graham"
title = "Episode 2"
subtitle = "Starting the AntiRSI port."
feature = "image/ep2.png"
+++

In episode 2, we dive into porting an app from Mac OS X to GNUstep, and shave quite a few yaks along the way. The app is [AntiRSI](https://github.com/srbaker/antirsi), and the yaks include updating the GNUstep libraries, and trying out the `buildtool` command for building Xcode projects on Linux.

The video of this episode is [available for replay](https://replay.objc-retain.com/u/graham/m/episode-2/), and mirrored on [Peertube](https://peertube.co.uk/videos/watch/2089f9b6-dc3b-4d1a-ae1b-397e58ea9171) and [Youtube](https://youtu.be/O6vAkR2LZrA). Please follow us on Twitch to get notified next time we go live, and consider [becoming a patron](https://patreon.com/iamleeg) to support this and other streams!
