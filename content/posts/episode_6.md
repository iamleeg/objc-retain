+++
date = 2021-03-24T22:00:00Z
lastmod = 2021-07-12T07:20:00Z
author = "graham"
title = "Episode 6"
subtitle = "AntiRSI runs!"
feature = "image/ep6.png"
+++

We discuss the 20th anniversary of OS X; gcc and LLVM; Swift and GNUstep; and whether Rails needs to adopt the GPL.

Then we make the one small fix to AntiRSI that makes it run, and work! We need to fix up the Dock icon drawing, which we diagnose this time and will fix next time.

You can watch on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-6/); the video is also mirrored on [Peertube](https://peertube.co.uk/videos/watch/44e96c73-cd33-4422-a484-4775fce267e4) and [Youtube](https://youtu.be/d_w1QFcKO90). To get notified when we next stream and to join in the live chat, please [follow on twitch](https://twitch.tv/objcretain)!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
