+++
date = 2021-09-16T11:00:00Z
lastmod = 2021-09-16T11:00:00Z
author = "graham"
title = "Episode 24"
subtitle = "We submit the simpleagenda pull request."
feature = "image/ep24.png"
+++

We start by talking about Steven's adventures packaging GNUstep for Debian, and the different philosophies around package maintenance. Does it make sense for GNUstep to be part of the OS, or a "vendor" package, or bundled into apps? The answer to any can be "yes" dependent on context.

And then we look over our work on simpleagenda to date, and decide to just file the pull request. We aren't sure whether this will be accepted or not so we discuss some "plan B" projects for if we're blocked on the next episode awaiting review.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-24).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
