+++
date = 2021-09-22T06:00:00Z
lastmod = 2021-09-22T06:00:00Z
author = "graham"
title = "Episode 25"
subtitle = "Buildtool; Renaissance; Refactoring."
feature = "image/ep25.png"
+++

We note Greg Casamento's release of the first versions of [The GNUstep Xcode Library](https://github.com/gnustep/libs-xcode) and [buildtool](https://github.com/gnustep/tools-buildtool), software for working with Xcode projects when away from a Mac. The we discuss Steven's use of [Renaissance](https://github.com/gnustep/libs-renaissance) for generating UIs from XML descriptions, and the fact that he has a couple of NeXT workstations on the way.

Then we get back to SimpleAgenda. Philippe had some [comments on our calendar discovery PR](https://github.com/poroussel/simpleagenda/pull/10#issuecomment-919774090) so we start working through those.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-25).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
