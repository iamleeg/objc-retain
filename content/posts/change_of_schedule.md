+++
date = 2021-03-24T22:05:00Z
lastmod = 2021-03-24T22:05:00Z
author = "graham"
title = "Change of Schedule next week"
subtitle = "Tuesday 30th March, 2000UTC"
feature = "image/objcretain.png"
+++

Episode 7 will be Tuesday 30th March, not Wednesday 31st. 2000 UTC due to the daylight savings change in the UK and Europe!
