+++
date = 2022-03-25T07:00:00Z
lastmod = 2022-03-25T07:00:00Z
author = "graham"
title = "Episode 38"
subtitle = "Protestware; Xcake; XCTest."
feature = "image/ep38.png"
+++

We react to the news on the "protestware" npm packages, and the legal dispute between Neo4J and PureThink over claims that a product is "free and open source". Then we note that our previous goal—a command-line tool to build Xcode projects on Linux—is already done, because we can use the XCake gem. So we define a new goal: building a desktop search application, and use that to drive further advances in Xcode compatibility.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-38).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
