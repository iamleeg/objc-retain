+++
date = 2021-02-24T21:00:00Z
lastmod = 2021-07-11T12:30:00Z
author = "graham"
title = "Episode 3"
subtitle = "Debugging GNUstep-make."
feature = "image/ep3.png"
+++

This was meant to be the "continuing the AntiRSI port" post, and actually we were doing OK at that, but somewhere along the line I hosed the GNUstep install so it turned into "working out how `gnustep-make` finds the base library". That's actually pretty interesting debugging content, so we left it all there!

The video of this episode is on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-3/) and mirrored at  [Peertube](https://peertube.co.uk/videos/watch/f77c5e8c-8448-4e76-a8c7-382b55aaa69f) and [Youtube](https://www.youtube.com/watch?v=MCMZyuempQE). Please [follow us on Twitch](https://twitch.tv/objcretain) to get notified next time we go live, and consider [becoming one of Graham's patrons](https://patreon.com/iamleeg) to support this and other streams!
