+++
date = 2021-06-10T17:00:00Z
lastmod = 2021-07-16T13:00:00Z
author = "graham"
title = "Episode 16"
subtitle = "Zero to GNUstep."
feature = "image/ep16.png"
+++

Having reinstalled the computer from last time, we go about setting up GNUstep for development from a stock Debian Bullseye install.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-16/). It's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/c14092c0-d41e-4c83-953d-885cd3e67521) and [Youtube](https://youtu.be/YK84tHwln1w).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
