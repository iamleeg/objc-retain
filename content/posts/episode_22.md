---
title: "Episode 22"
date: 2021-09-01T07:05:39+01:00
draft: false
subtitle: "Significant progress on SimpleAgenda!"
feature: "image/ep22.png"
author: graham
---

We open with a recap on what's been going on over the month since our last episode, including Steven's work on the GNUstep website and Debian packages for GNUstep. We discuss Project Eden, a planned "garden without walls" for distributing Free Software GNUstep and Cocoa apps.

And then we dig into SimpleAgenda once again, and polish off the feature that we've been working on. It'll now discover your calendar by following breadcrumbs from the top-level of your caldav server, making it much easier to configure. This work is nearly complete and ready for review: we'll be looking at some improved error handling next time before submitting a PR!

This episode starts with a quick discussion of the fact that Twitch didn't appear to pick up our stream. On completion, we found that Twitch had received the entire stream from our server, it just hadn't broadcast most of it or sent notifications that we were live. If you watch via Twitch you probably missed this episode, for which we apologise. You can watch it back on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-22/).


Unfortunately solving this problem is out of our hands as Twitch is a proprietary service that we are not free to study or improve. We recommend watching live via the [live player](https://rtmp.objc-retain.com). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
