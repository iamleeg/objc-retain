+++
date = 2021-09-22T06:01:00Z
lastmod = 2021-09-22T06:01:00Z
author = "graham"
title = "Week of objc"
subtitle = "Four episodes on the last four days of September 2021!"
+++

We're gearing up for a special week next week at `[objc retain];` where we have an episode on Monday, Tuesday, Wednesday _and_ Thursday! The secret internal reason for this is to meet some Twitch criteria to allow ad-free subscription to [our channel](https://twitch.tv/objcretain) but also we're very happy to get to write Objective-C and share our love of GNUstep with the community, and this is a chance to do more of that.

These episodes will be at the earlier time of 14:00-16:00 UTC, on the 27-30 September. Join us, if you can, we'd love to chat with you! Of course you don't have to choose Twitch if you don't want: check in to the [live player](https://rtmp.objc-retain.com), [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) all through fully free software.

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes. See you for week of objc!
