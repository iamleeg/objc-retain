+++
date = 2021-04-14T22:00:00Z
lastmod = 2021-07-14T22:00:00Z
author = "graham"
title = "Episode 9"
subtitle = "Software freedom; autonomy; SimpleAgenda."
feature = "image/ep9.png"
+++

We talk a bit (quite a bit) about the ideas of software freedom and autonomy, and my Mac that has been returned from the dead. Then we get into the exploratory phase of our next project: adding some features to SimpleAgenda.

You can watch again on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-9/). The video is also mirrored to  [Peertube](https://peertube.co.uk/videos/watch/05c2dedb-7880-4c4c-8346-f2c48f772e08) and [Youtube](https://youtu.be/_89kdriLm5s).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch past episodes on [the replay server](https://replay.objc-retain.com). And you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
