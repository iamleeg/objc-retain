+++
date = 2021-03-30T21:00:00Z
lastmod = 2021-07-12T15:00:00Z
author = "graham"
title = "Episode 7"
subtitle = "Why NeXT was special; AntiRSI continues"
feature = "image/ep7.png"
+++

We talk about what made NeXT special, and why we want a free software platform for our Cocoa apps. And then the port of AntiRSI continues as we diagnose some Cocoa Bindings problems.

<s>We're back to our regular schedule next week</s> We'll be live at 1800UTC on Tuesday 6th April next week, back to Wednesdays thereafter.

You can watch on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-7/); the video is also mirrored to [Peertube](https://peertube.co.uk/videos/watch/49c033e0-1269-4032-9517-81ba1fde0673) and [Youtube](https://youtu.be/E4WGqwLDrx4). To get notified when we next stream and to join in the live chat, please [follow on twitch](https://twitch.tv/objcretain)! However if you'd rather watch on a fully-free server, [you can watch on the video server](https://rtmp.objc-retain.com) when we go live.

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
