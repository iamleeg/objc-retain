+++
date = 2021-04-05T18:00:00Z
lastmod = 2021-04-05T18:00:00Z
author = "graham"
title = "Free Software Chat"
subtitle = "Webchat via mattermost, or use an IRC client."
+++

You can now chat with the [objc retain]; community via Mattermost at [chat.objc-retain.com](https://chat.objc-retain.com). We're in `#stream-chat` when we're live, which is also bridged to Twitch. Hang out with the community at other times in the other channels.

See [the contact page](/contact) for info about connecting with TalkSoup or any other IRC client.

Providing a free software [video player](https://rtmp.objc-retain.com) and [chat server](https://chat.objc-retain.com) adds significantly to the operational costs of the stream. We still do not intend to charge for direct access to the content, but if you'd like to contribute toward our costs please become one of [Graham's patrons](https://patreon.com/iamleeg).