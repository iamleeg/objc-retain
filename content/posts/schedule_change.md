+++
date = 2021-03-31T21:00:00Z
lastmod = 2021-03-31T21:00:00Z
author = "graham"
title = "Schedule_change"
subtitle = "Tuesday April 6th at 1800UTC"
feature = "image/agenda.png"
+++

I made a mistake in the [last post](https://objc-retain.com/episode_7/) and said we would be back to streaming on Wednesday. We are not: next week's stream will also be on Tuesday, April 6th. We start at 1800UTC (1900BST, 2000EDT, some other time in other places) and typically go for two hours. You can watch [here](https://twitch.tv/objcretain) or [here](https://rtmp.objc-retain.com).

Apologies for the confusion. From the _following_ week, Wednesday April 14th, we go back to 1800UTC on Wednesdays for the foreseeable future (which as we now know, I do not foresee particularly far into).
