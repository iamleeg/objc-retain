+++
date = 2022-02-25T08:00:00Z
lastmod = 2022-02-25T08:00:00Z
author = "graham"
title = "Episode 37"
subtitle = "Team commitment; Cocoapods; Xcodeproj."
feature = "image/ep37.png"
+++

We start, as ever, by talking about whatever is on our minds. In this case it's about whether software engineers are committed to their users, their tech communities or vendors, their employers, their customers, their own careers…and what happens when these commitments come into conflict.

Getting back to the code, we review our plan to see what it is we are meant to be working on. It's CocoaPods for GNUstep! The problem is, CocoaPods works (or at least 100% of the tests pass) on our GNUstep/Linux setup already, so there's not much to do! We realise that what there _is_ to do is to make it easier for GNUstep folks to work with Xcode project files so that they can integrate with CocoaPods.

Those CocoaPods folks have a ruby gem for working with Xcode project files, so we start adding the features we'll need to build an Xcode workflow on GNUstep platforms.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-37).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
