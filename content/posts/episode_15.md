+++
date = 2021-06-10T17:00:00Z
lastmod = 2021-07-16T08:20:00Z
author = "graham"
title = "Episode 15"
subtitle = "Disaster Recovery."
feature = "image/ep15.png"
+++

We discuss what we'd like from a fresh install of a GNUstep developer system, as the SSD on our streaming box showed signs of failure just before we went live.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-15/). The video is mirrored to [PeerTube](https://peertube.co.uk/videos/watch/cc00e9ec-3626-4dec-bd06-2ef4106ce200) and [Youtube](https://youtu.be/JGG0T_ubY8E).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
