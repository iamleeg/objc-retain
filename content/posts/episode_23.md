+++
date = 2021-09-08T15:00:00Z
lastmod = 2021-09-08T15:00:00Z
author = "graham"
title = "Episode 23"
subtitle = "Debian and Ubuntu; Error checking in SimpleAgenda"
feature = "image/ep23.png"
+++

It turns out that Twitch have created a category for Software & Games Development, so we moved the stream into that category live. We also discussed updates to Steven's efforts on packaging GNUstep for Debian; the difference between Debian and Ubuntu; and the difference between Canonical and Red Hat.

Then we get back on to the pull request for SimpleAgenda we're working on. Because this is network code dealing with unsafe data from t'internet, we want to be sure that all potential failure cases and errors are handled correctly, and that all objects created are cleaned up.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-23).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
