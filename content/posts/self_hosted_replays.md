---
title: "Self-hosted Replays"
author: "graham"
date: 2021-07-12T08:07:24+01:00
feature: image/replay.png
draft: false
---

We now have our own [replay server](https://replay.objc-retain.com), and I'm gradually uploading and transcoding the videos to appear there. Stream replay videos will appear in [this collection](https://replay.objc-retain.com/u/graham/collection/stream-replays/); at time of writing we're processing Episode 7. The next to record is episode 20, so it'll be a while before we're in sync.

Once we are caught up, [replay.objc-retain.com](https://replay.objc-retain.com) will be the canonical home for stream replays and sharing to Peertube and Youtube will be best-effort. The replay server is hosted using [GNU MediaGoblin](http://mediagoblin.org/) and [nginx](http://nginx.org) on [Debian GNU/Linux](https://debian.org).

If you struggle to join us live and would prefer to always watch the replays, you may want to [subscribe to the collection's atom feed](https://replay.objc-retain.com/u/graham/collection/stream-replays/atom/) to get notified when new episode replays become available.

Thanks for your support!