+++
date = 2021-05-21T11:00:00Z
lastmod = 2021-05-21T11:00:00Z
author = "default"
title = "Apology"
subtitle = "Sorry about the delay in getting replays up!"
+++

It's been over two weeks between recording episode 12, and getting the replays for episodes 12-14 up here. I'm sorry for that.

We've been having difficulty with the availability of the peer tube instance where we host the replays, so I hadn't been able to upload the latest episodes for some time and as a result chose not to update the feed. I'm going to create a GNU MediaGoblin server for the replays soon, then will put all the episodes up there and announce here when it's available.

Of course this adds to the running costs, so if you're able please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg), and help cover some of the hosting costs of all of this.
