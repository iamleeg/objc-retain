+++
date = 2022-01-27T22:00:00Z
lastmod = 2022-01-27T22:00:00Z
author = "graham"
title = "Episode 35"
subtitle = "A lot of random chatting."
feature = "image/ep35.png"
+++

We start off very excited with our new project to write an XCTest-compatible testing library, then talk so much about the community and our places in it that we don't have much time to devote to the project. C'est la vie.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-35).

### Important Infrastructure Announcement

After a lot of difficulty keeping the stream up and running, I have decommissioned the RTMP server and Mattermost chat. While both made it possible to engage with the stream without using the proprietary twitch service, the fact is that most of the community _were_ using twitch and that keeping that stuff running properly was a burden both in time and in money. The chat in particular was not well used, with most people preferring to chat live during the stream in twitch.

This blog, and the [GNU MediaGoblin replay server](https://replay.objc-retain.com), remain available.

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
