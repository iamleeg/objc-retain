+++
date = 2021-06-15T20:00:00Z
lastmod = 2021-06-15T20:00:00Z
author = "graham"
title = "Episode 17"
subtitle = "We re-learn about caldav"
feature = "image/ep17.png"
+++

Having broken and remade everything we get back up to speed with the caldav protocol, and the work we need to do to make SimpleAgenda auto-discover your calendar.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-17/). It's mirrored to [PeerTube](https://peertube.co.uk/videos/watch/26fa7a22-dc1f-487f-91d4-c167a5f437fe) and [Youtube](https://youtu.be/JCfR3fnCdzk).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
