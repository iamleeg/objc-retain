+++
date = 2022-05-12T21:00:00Z
lastmod = 2022-05-12T21:00:00Z
author = "graham"
title = "Episode 40"
subtitle = "More on XCake and buildtools."
feature = "image/ep40.png"
+++

After our customary hour of chat, this time mostly about retrocomputing, we get back into working with XCake to generate Xcode projects. We realise that while XCake doesn't document this, it _can_ make command-line tool projects, but we have some difficulty with the generated project.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-40).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
