+++
date = 2021-05-12T21:00:00Z
lastmod = 2021-07-15T08:00:00Z
author = "default"
title = "Episode 13"
subtitle = "We actually fix something!"
feature = "image/ep13.png"
+++

We abandon the mess that was our work on the duration UI in SimpleAgenda and try to solve a real bug that somebody really reported, in its synchronisation with Nextcloud. This actually goes somewhere, and we put up a pull request!

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-13/). It's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/4c77b92d-990d-4738-9085-17b58bf7cfb6) and [Youtube](https://youtu.be/1zjKt1rxiow).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
