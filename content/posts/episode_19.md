+++
date = 2021-07-06T21:00:00Z
lastmod = 2021-07-19T11:45:00Z
author = "graham"
title = "Episode 19"
subtitle = "Copilot, free software, getting started with ObjC."
feature = "image/ep19.png"
+++

We spend most of the episode talking about github's place in the free software world, and answer a question from the chat about getting started with Objective-C in 2021. We recommend the [GNUstep library site](http://gnustep.made-it.com), among other resources.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-19/), and it's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/6a7d3577-3b0e-490b-b953-0f159902e96f) and [YouTube](https://youtu.be/VevutcI9AVE).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [watch the replays](https://replay.objc-retain.com) and [join in the chat](https://chat.objc-retain.com) at any time. The schedule has been pretty variable over the last few weeks so check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
