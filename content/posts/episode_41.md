+++
date = 2022-06-30T22:00:00Z
lastmod = 2022-06-30T22:00:00Z
author = "graham"
title = "Episode 41"
subtitle = "Ta-ta for now!"
feature = "image/ep41.png"
+++

After a long hiatus caused by scheduling conflicts, we come abck to say that we won't be able to stream again in the foreseeable future. Unfortunately work commitments mean that we have to put the stream on indefinite pause. We look at what we've achieved since we started in January 2021, and who else is streaming and contributing to the GNUstep community.

We also discuss the Software Freedom Conservancy's [#GiveUpGitHub](https://t.co/X7aJLPOIxy) campaign, and our favourite computer aesthetic: the G3-era Macs.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-41).

To get notified of future episodes, [follow on Twitch](https://twitch.tv/objcretain) and when we start up again you'll be first to hear about it. And you can [watch replays](https://replay.objc-retain.com) at any time. Thanks for your support!