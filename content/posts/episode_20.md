+++
date = 2021-07-13T20:00:00Z
lastmod = 2021-07-19T16:30:00Z
author = "graham"
title = "Episode 20"
subtitle = "Network effects; well-known caldav."
feature = "image/ep20.png"
+++

We talk a bit about how the proprietary version of a thing is often easier in deployment or use than the free version. This is particularly motivated by my work replicating Twitch in fully-free software.

Then we go back to SimpleAgenda, and get the first stage of calendar discovery in caldav implemented!

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-20/). It's also mirrored to  [PeerTube](https://peertube.co.uk/videos/watch/c9fceec8-24ca-4302-ae34-a392aea2d009) and [YouTube](https://youtu.be/fZTt90nhyxg).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
