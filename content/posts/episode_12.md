+++
date = 2021-05-12T21:00:00Z
lastmod = 2021-07-14T22:32:00Z
author = "default"
title = "Episode 12"
subtitle = "I break everything."
feature = "image/ep12.png"
+++

As is often the case, if you let me anywhere near a computer I'll mess it up for you. We get distracted from fixing a SimpleAgenda bug by trying to fix another bug, and introduce even more bugs as a result.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-12/). It's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/37be2d82-d015-4d8b-aa8f-0b2369704fb4) and [Youtube](https://youtu.be/Ea185iyQMLM).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
