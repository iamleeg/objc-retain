+++
date = 2021-06-29T20:00:00Z
lastmod = 2021-07-17T20:00:00Z
author = "graham"
title = "Episode 18"
subtitle = "Fix the fscking manual"
feature = "image/ep18.png"
+++

We talk a bit about how we've got more free software, but less software freedom, than ever before. We look into the networking fixes we need to make to SimpleAgenda, but along the way find a documentation bug in GNUstep-base so fix that instead.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-18/). It's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/8cdf5c33-77a3-4314-90cf-952bc35d92fe) and [YouTube](https://youtu.be/-QDsiON1Gac).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
