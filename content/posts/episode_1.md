+++
date = 2021-02-10T21:00:00Z
lastmod = 2021-07-11T12:30:00Z
author = "graham"
title = "Episode 1"
subtitle = "We did our first episode!"
feature = "image/ep1.png"
+++

This may seem like deja vu as we already told you about our [first stream](./first_stream), but this was the first proper episode of `[objc retain]`!

In it, Steven and Graham introduce themselves, we talk a little about the history of Objective-C, NeXT, Cocoa and GNUstep, then we work through a Cocoa tutorial to see how GORM and ProjectCenter differ from Xcode.

If you were there, and you enjoyed it, please tell your friends and colleagues to come along next time: 1900UTC on Wednesday 17 Feb. If you weren't there, please come along next time, and bring your friends and colleagues!

The video is available [on the replay server](https://replay.objc-retain.com/u/graham/m/episode-1/), and mirrored at [Peertube](https://peertube.co.uk/videos/watch/a4640de5-8896-4b3a-86cc-be7e67a27adb) and [Youtube](https://www.youtube.com/watch?v=1Jhw7j3KckA). You can hear at the beginning that I confidently tell Steven my microphone can't be heard on the recording, whoops! This was our first episode, it does get better!

See you next time!
