+++
date = 2022-04-15T06:00:00Z
lastmod = 2022-04-15T06:00:00Z
author = "graham"
title = "Episode 39"
subtitle = "More on Xcode and XCake."
feature = "image/ep39.md"
+++

After our customary hour of chat, this time mostly about the mobile platforms and the web, we get back into working with XCake to generate Xcode projects. We're trying to build the GNUstep port of XCTest, and we make good progress!

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-39).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [watch replays](https://replay.objc-retain.com) at any time. Check in at twitch for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
