+++
date = 2021-03-03T21:00:00Z
lastmod = 2021-07-11T12:30:00Z
author = "graham"
title = "Episode 4"
subtitle = "Windows and other platforms."
feature = "image/ep4.png"
+++

Steven and I discuss the various platforms that GNUstep supports, and the availability of packages for Windows. We then try to build GNUstep for Windows following the instructions on the wiki. The video is on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-4/) and mirrored at [Peertube](https://peertube.co.uk/videos/watch/5abc1d5e-917b-4be0-902c-eba14a4d9356) and [Youtube](https://youtu.be/Nep8QBJXGI4).

By the way, there's no stream next week: the next episode is Wednesday, March 17th. And we're back on Unix for that one! Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
