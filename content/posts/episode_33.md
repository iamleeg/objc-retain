+++
date = 2021-11-30T21:00:00Z
lastmod = 2021-11-30T21:00:00Z
author = "graham"
title = "Episode 33"
subtitle = "Podcast; Addresses."
feature = "image/ep33.png"
+++

Sorry for the hiatus. We have had two difficulties: one is scheduling, and the other is a never-ending succession of technical difficulties that annoy me to the point where I don't want to try going live. Today, after a rocky start, we got going.

We started with a discussion of my interview with Leo Dion at the [empower apps podcast](https://www.empowerapps.show/109). This was a fun episode, where we discussed what Swift programmers can learn by picking up Objective-C.

Then we turned our attention to coding. Our goal is carddav support in Addresses, which we successfully descoped to just building a carddav integration. Then we yak shaved trying to build a test tool, and ended up writing a pull request for GNUstep-base.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-33).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
