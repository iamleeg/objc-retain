+++
date = 2021-05-21T11:00:00Z
lastmod = 2021-05-21T11:00:00Z
author = "graham"
title = "Episode 14"
subtitle = "Some Research."
feature = "image/ep14.png"
+++

We need to learn more about Caldav before we continue to work on simple agenda, so that's what we do.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-14/), and it's also mirrored to [PeerTube](https://peertube.co.uk/videos/watch/fd1d1871-2d8e-48f9-887d-1f42fcb77e70) and [Youtube](https://www.youtube.com/watch?v=QFKnGMA7b5Y).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com) at 1830UTC on Wednesdays, or [follow on Twitch](https://twitch.tv/objcretain). You can always watch [the replays](https://replay.objc-retain.com), and you can [join in the chat](https://chat.objc-retain.com) at any time. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
