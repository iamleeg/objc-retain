---
title: "Episode 21"
date: 2021-08-31T21:10:39+01:00
draft: false
subtitle: "Bumper cross-over episode with Dos Amigans."
feature: "image/ep21.png"
author: graham
---

This replay went up very late because I went on holiday for a couple of weeks then caught Coronavirus then went heads-down on my PhD, but this is the bumper summer special episode!

Steven and I crossed the streams and did an episode with [Amiga content](https://dosamigans.tv) in addition to the Objective-C stuff. We have a quick go on a [Commodore Plus 4](https://dfarq.homeip.net/commodore-plus-4-commodore-16/), one of many things CBM tried that didn't quite succeed.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-21/).

For future episodes, check in to the [live player](https://rtmp.objc-retain.com), or [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
