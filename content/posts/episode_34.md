+++
date = 2022-01-13T23:00:00Z
lastmod = 2022-01-13T23:00:00Z
author = "graham"
title = "Episode_34"
subtitle = "Licenses aren't sufficient; clown computing; unit tests."
feature = "image/ep34.png"
+++
Welcome to 2022! It's just as bad as 2021, so far!

We started with a discussion of how focus on the licensing parts of software freedom limits our ability to give people those freedoms. Then we look at our CardDAV problem, and decide to shave another yak before we get started.

You can watch this episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-34).

For future episodes, [follow on Twitch](https://twitch.tv/objcretain). And you can [join in the chat](https://chat.objc-retain.com) and [watch replays](https://replay.objc-retain.com) at any time. Check in here and on the chat for stream announcements. See you soon!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
