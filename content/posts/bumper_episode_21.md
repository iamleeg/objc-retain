---
title: "Bumper episode 21!"
subtitle: "We're crossing the streams!"
date: 2021-07-21T12:41:37+01:00
draft: false
author: graham
feature: "image/da.png"
---

We missed the regularly-scheduled Episode 21. Sorry for that, we had a plan to go live but the heatwave did in for my streaming computer and it wouldn't stay up.

To make up for that, we are planning a _bumper four-hour stream-crossing spectacular_ tomorrow, Thursday July 22 2021! Join us [live](https://rtmp.objc-retain.com) at 1700 UTC (1800 BST, 1900 EDT) as we stream simultaneously to [objc retain](https://twitch.tv/objcretain) and [Dos Amigans](https://twitch.tv/dosamigans) for a fun special covering Amigas, Free Software, Objective-C, and maybe a little extra surprise.

See you there!
