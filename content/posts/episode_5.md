+++
date = 2021-03-17T23:00:00Z
lastmod = 2021-07-11T22:40:00Z
author = "graham"
title = "Episode 5"
subtitle = "Thoughts on Swift; AntiRSI builds!"
feature = "image/ep5.png"
+++

After a couple of patches to [`libs-xcode`](https://github.com/gnustep/libs-xcode) and with another PR to `gnustep-gui` threatened, AntiRSI finally builds! It doesn't run, and we'll get to why on the next episode.

But that's the _second_ hour of the stream. In the first, Steven and I (mostly Steven) explain our opinions on the Swift programming language, and why we've stuck with ObjC despite the new features and capabilities on offer.

You can watch on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-5/); the video is also mirrored to  [Peertube](https://peertube.co.uk/videos/watch/b222b18b-be84-4235-b222-8c7a49ec959b) and [Youtube](https://www.youtube.com/watch?v=hvEviXLErB8). To get notified when we next stream and to join in the live chat, please [follow on twitch](https://twitch.tv/objcretain)!

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
