+++
date = 2021-04-06T20:00:00Z
lastmod = 2021-07-12T23:18:00Z
author = "graham"
title = "Episode 8"
subtitle = "AntiRSI done; what next?"
feature = "image/ep8.png"
+++


We discuss the latest twist in Oracle v Google and then get into AntiRSI. With some help from Josh Freeman, we finish the live app icon drawing in AntiRSI and discuss another project to get into.

We are pretty confident that next time we'll be in the regular scheduled slot of Wednesday, 1800 UTC!

You can watch the episode on [the replay server](https://replay.objc-retain.com/u/graham/m/episode-8/), and it's mirrored at [Peertube](https://peertube.co.uk/videos/watch/574565b6-5349-4303-9041-1bb3442c03f5) and [Youtube](https://youtu.be/PfJU2tbcMP0).

To get notified when we next stream and to join in the live chat, please [join our mattermost](https://chat.objc-retain.com)! You can watch [on the video server](https://rtmp.objc-retain.com) when we go live, or if you'd prefer [follow on twitch](https://twitch.tv/objcretain).

Also please consider becoming one of [Graham's patrons](https://patreon.com/iamleeg) or [Steven's patrons](https://patreon.com/srbaker), and help cover some of the hosting costs of all of this and support creating new episodes!
