+++
date = 2021-03-30T21:00:00Z
lastmod = 2021-03-30T21:00:00Z
author = "graham"
title = "Free Software streaming"
subtitle = "An alternative to Twitch."
+++

You may not be comfortable with using a proprietary service like Twitch to watch a video stream on Free Software. You can now connect to our streaming server, which uses fully free software, directly to watch the stream. When we're live, [browse to rtmp.objc-retain.com](https://objc-retain.com) on your favourite device.

We're working on bringing other features to you in a fully-unencumbered way. Chat is coming up next, check back here for details. Of course this adds to the running costs of the stream significantly so if you're able please consider [becoming a patron](https://patreon.com/iamleeg) to support our work of promoting software freedom.
