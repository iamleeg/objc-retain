---
title: "Contact"
date: 2021-01-21T17:21:24Z
draft: false
---

You're always welcome to hang out with us [in our Mattermost server](https://chat.objc-retain.com)! We only use the `objc-retain` team, and we use the `#stream-chat` channel when we're live and the other rooms whenever we like.

## Connecting via Twitch

If you watch the stream in Twitch, then the `#stream-chat` channel on Mattermost is always bridged to the Twitch chat. The bridge is bidirectional, so you can join in over on Twitch without missing out on the conversation.

## Connecting via IRC

You might want to use an IRC client to connect to chat, such as [TalkSoup](http://gap.nongnu.org/talksoup/index.html#downloads). This is possible using these steps:

 1. Sign up for an account [at Mattermost](https://chat.objc-retain.com).
 2. Log in, and go to the Security tab in Account Settings.
 3. Click "Edit" next to Personal Access Tokens, and then "Create Token".
 4. Copy the token! You won't be able to see it again!
 5. Connect your IRC client to `chat.objc-retain.com:6667`
 6. On connection, authenticate by sending your token to mattermost. Replace {username} with your user name, e.g. `leeg`, and {token} with your personal access token.
 
    /msg mattermost login chat.objcretain.com objc-retain {username} {token}

This should join you to all the channels. We use `#stream-chat` when we're live (and it's bridged to Twitch) but feel welcome to hang out anywhere.
